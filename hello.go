package hello

import (
	"fmt"
	"strings"
)

// Say returns message with Hello prefix
func Say(name string) string {
	if name == "" {
		name = "world"
	}

	name = strings.ToLower(name)
	name = strings.Title(name)

	return fmt.Sprintf("Hello %s", name)
}
