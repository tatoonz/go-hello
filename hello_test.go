package hello_test

import (
	"testing"

	hello "gitlab.com/tatoonz/go-hello"
)

func TestHelloWithDefaultName(t *testing.T) {
	out := hello.Say("")
	if out != "Hello World" {
		t.Errorf("it should return `Hello World`, but get %v", out)
	}
}

func TestHelloWithInput(t *testing.T) {
	out := hello.Say("tOoN")
	if out != "Hello Toon" {
		t.Errorf("it should return `Hello Toon`, but get %v", out)
	}
}
